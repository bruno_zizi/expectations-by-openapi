# Description #

This is a practical example on how to create expectation for MockServer starting by an OpenAPI v3 schema 
(former Swagger).

The project consists of two docker containers, the first running an instance of Nginx and the other
running MockServer.
The Nginx instance behaves like a generic service or application; it is configured as a reverse proxy 
(it routes all requests to MockServer) and is accessible through port 80.

The MockServer instance simulates a weather data provider [https://openweathermap.org/api](https://openweathermap.org/api)

## Project structure ##

* `nginx.conf` the Nginx configuration file
* `mock-configuration/initializerJson.json` the EMPTY file containing MockServer expectations
* `mock-configuration/mockserver.properties` mockserver standard configuration file
* `docker-compose.yaml` docker compose file
`mock-configuration/openapi-specs.yaml` the OpenAPI v3 schema containing the API specifications



## Starting up the application ##
 `$ docker compose up`


## Checking the MockServer status ##

Open [http://localhost:1080/mockserver/dashboard](http://localhost:1080/mockserver/dashboard) in a web browser. 

There should be no expectations.


## Creating expectations ##

Let's make MockServer create all the expectations for us by providing the OpenAPI schema file

    curl -X PUT --location "http://localhost:1080/mockserver/openapi"  -H "Content-Type: application/json" -d "{ \"specUrlOrPayload\": \"file:/config/openapi-specs.yaml\"}"

Now the file `mock-configuration/initializerJson.json` should contain all the generated expectations.



## Testing the application ##

    curl -X GET --location "http://localhost/data/2.5/weather?lat=35&lon=139&appid=c14cc89851b2b90933f8862e2ed82a05" -H "Accept: application/json"
    curl -X GET --location "http://localhost/data/2.5/onecall?lat=35&lon=139&appid=c14cc89851b2b90933f8862e2ed82a05" -H "Accept: application/json"

or just open in a web browser the following links

[http://localhost/data/2.5/weather?lat=35&lon=139&appid=c14cc89851b2b90933f8862e2ed82a05](http://localhost/data/2.5/weather?lat=35&lon=139&appid=c14cc89851b2b90933f8862e2ed82a05)


[http://localhost/data/2.5/onecall?lat=35&lon=139&appid=c14cc89851b2b90933f8862e2ed82a05](http://localhost/data/2.5/onecall?lat=35&lon=139&appid=c14cc89851b2b90933f8862e2ed82a05)



### Questions? ###

If you have any question or if you want to report any issue/bug please contact the 
author at [bruno.zizi@cloudacademy.com](mailto:bruno.zizi@cloudacademy.com)